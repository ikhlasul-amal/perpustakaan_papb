package com.example.restapi195150401111029;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BukuAdapter extends RecyclerView.Adapter<BukuAdapter.ViewHolder> {
    LayoutInflater inflater;
    ArrayList<Buku> items;
    Context context;

    public BukuAdapter(Context context, ArrayList<Buku> items){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.row, parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Buku item = items.get(position);
        holder.tvjudul.setText(item.getJudul());
        holder.tvdeskripsi.setText(item.getDeskripsi());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvjudul;
        TextView tvdeskripsi;

        public ViewHolder (@NonNull View itemView){
            super(itemView);
            tvjudul = itemView.findViewById(R.id.tvjudul);
            tvdeskripsi = itemView.findViewById(R.id.tvdeskripsi);
        }
    }

}
