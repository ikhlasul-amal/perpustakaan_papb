package com.example.restapi195150401111029;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.restapi195150401111029.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    ArrayList<Buku> listBuku = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.rv1.setLayoutManager(new LinearLayoutManager(this));
        final BukuAdapter adapter = new BukuAdapter(this, listBuku);
        binding.rv1.setAdapter(adapter);
        PerpusService perpusService = RetrofitClient.getClient().create(PerpusService.class);
        getData(perpusService, adapter);

        binding.bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Buku> addBuku = perpusService.create(null, binding.etJudul.getText().toString(), binding.etDeskripsi.getText().toString());

                addBuku.enqueue(new Callback<Buku>() {
                    @Override
                    public void onResponse(Call<Buku> call, Response<Buku> response) {
                        binding.etJudul.setText("");
                        binding.etDeskripsi.setText("");
                        getData(perpusService, adapter);
                    }

                    @Override
                    public void onFailure(Call<Buku> call, Throwable t) {
                        Log.d("error", "" + t.getMessage());
                        Toast.makeText(getApplicationContext(), "error : " + t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void getData(PerpusService service, BukuAdapter adapter){
        Call<List<Buku>> listRequest = service.listBuku();
        listRequest.enqueue(new Callback<List<Buku>>() {
            @Override
            public void onResponse(Call<List<Buku>> call, Response<List<Buku>> response) {
                if (response.isSuccessful()){
                    List<Buku> list = response.body();
                    Log.d("success", "list : " + list.size());
                    listBuku.clear();
                    listBuku.addAll(list);
                    adapter.notifyDataSetChanged();
                } else {
                    Log.d("error", "" + response.errorBody());
                    Toast.makeText(MainActivity.this, "" + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Buku>> call, Throwable t) {
                Log.d("error", "" + t.getMessage());
                Toast.makeText(getApplicationContext(), "error : " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}